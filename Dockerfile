FROM alpine:3.11

# FROM nginx:1.18-alpine

ENV USER=root

ADD https://dl.bintray.com/php-alpine/key/php-alpine.rsa.pub /etc/apk/keys/php-alpine.rsa.pub

RUN apk --update-cache add ca-certificates && \
    echo "https://dl.bintray.com/php-alpine/v3.11/php-7.4" >> /etc/apk/repositories

RUN apk update

RUN apk add openrc && \
    apk add busybox-extras && \
    apk add nano && \
    apk add nginx && \
    apk add supervisor && \
    apk add curl && \
    rm /etc/nginx/conf.d/default.conf

RUN apk add \
    php \
    php-bcmath \
    php-dom \
    php-ctype \
    php-curl \
    php-fpm \
    php-gd \
    php-iconv \
    php-intl \
    php-json \
    php-mbstring \
    php-mysqlnd \
    php-opcache \
    php-openssl \
    php-pdo \
    php-pdo_mysql \
    php-pdo_pgsql \
    php-pdo_sqlite \
    php-phar \
    php-posix \
    php-session \
    php-soap \
    php-xml \
    php-xmlreader \
    php-zip 

# Configure nginx
COPY config/default.conf /etc/nginx/nginx.conf

# Configure PHP-FPM
COPY config/www.conf /etc/php7/php-fpm.d/www.conf
COPY config/php.ini /etc/php7/conf.d/custom.ini

# Configure supervisord
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Setup document root
RUN mkdir -p /var/www/html

# Make sure files/folders needed by the processes are accessable when they run under the nobody user
RUN chown -R $USER:$USER /var/www/html && \
    chown -R $USER:$USER /run && \
    chown -R $USER:$USER /var/lib/nginx && \
    chown -R $USER:$USER /var/log/nginx

# Add application
WORKDIR /var/www/html
COPY --chown=$USER src/ /var/www/html/

# Switch to use a non-root user from here on
USER $USER

# Expose the port nginx is reachable on
EXPOSE 8080

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]





