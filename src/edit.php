<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Environment Variable Dashboard</title>
</head>

<body class="edit">
    <div class="container">
        <div class="navbar">
            <h1 id="title">Environment Variable Dashboard</h1>
            <p id="credit">coded with &#10084;&#65039;&nbsp; by Infra Team</p>
        </div>
        <div class="php-area">
            <?php
            echo "<p>" . $_COOKIE['filename'] . "</p>";
            echo "<p><i>Last updated at " . date("d F Y H:i:s", filemtime('../env/' . $_COOKIE['filename'])) . "</i></p>";
            echo "<textarea class='form-control' cols=130 rows=20>" . file_get_contents('../env/' . $_COOKIE['filename']) . "</textarea>";

            if (isset($_POST['update'])) {
                $file = '../env/' . $_COOKIE['filename'];
                $target = '../env-backup/' . $_COOKIE['filename'] . "-copy";
                copy($file, $target);
                echo "file is copied";
            }
            ?>
        </div>
        <form method="post">
            <input class='btn btn-primary' type="submit" name="update" value="Update" />
        </form>
    </div>
    <script src="script.js"></script>
</body>

</html>