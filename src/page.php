<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
  <title>Environment Variable Dashboard</title>
</head>

<body class="page">
  <div class="container">
    <div class="navbar">
      <h1 id="title">Environment Variable Dashboard</h1>
      <p id="credit">coded with &#10084;&#65039;&nbsp; by Infra Team</p>
    </div>
    <div class="wrapper">
      <div class="file-name">
        <!-- This is where the Env Var name will be shown/created -->
      </div>
      <div class="edit-button"><a class="btn btn-primary" href="edit.php" role="button">Edit</a></div>
    </div>
    <div class="button-wrapper">
      <button type="button" class="btn btn-primary" id="sort-button">
        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-sort-alpha-down" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M4 2a.5.5 0 0 1 .5.5v11a.5.5 0 0 1-1 0v-11A.5.5 0 0 1 4 2z"></path>
          <path fill-rule="evenodd" d="M6.354 11.146a.5.5 0 0 1 0 .708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 0 1 .708-.708L4 12.793l1.646-1.647a.5.5 0 0 1 .708 0z"></path>
          <path d="M9.664 7l.418-1.371h1.781L12.281 7h1.121l-1.78-5.332h-1.235L8.597 7h1.067zM11 2.687l.652 2.157h-1.351l.652-2.157H11zM9.027 14h3.934v-.867h-2.645v-.055l2.567-3.719v-.691H9.098v.867h2.507v.055l-2.578 3.719V14z"></path>
        </svg>
      </button>
      <button type="button" class="btn btn-primary" id="sort-button-reversed">
        <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-sort-alpha-down-alt" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M4 2a.5.5 0 0 1 .5.5v11a.5.5 0 0 1-1 0v-11A.5.5 0 0 1 4 2z"></path>
          <path fill-rule="evenodd" d="M6.354 11.146a.5.5 0 0 1 0 .708l-2 2a.5.5 0 0 1-.708 0l-2-2a.5.5 0 0 1 .708-.708L4 12.793l1.646-1.647a.5.5 0 0 1 .708 0z"></path>
          <path d="M9.027 7h3.934v-.867h-2.645v-.055l2.567-3.719v-.691H9.098v.867h2.507v.055L9.027 6.309V7zm.637 7l.418-1.371h1.781L12.281 14h1.121l-1.78-5.332h-1.235L8.597 14h1.067zM11 9.687l.652 2.157h-1.351l.652-2.156H11z"></path>
        </svg>
      </button>
    </div>
    <?php
    echo "<p><i>Last updated at " . date("d F Y H:i:s", filemtime('../env/' . $_COOKIE['filename'])) . "</i></p>";
    ?>
    <table class="table table-striped">
      <!-- This is where the table will be created via JS -->
    </table>
  </div>
  <script src="script.js"></script>
</body>

</html>