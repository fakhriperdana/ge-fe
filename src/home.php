<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
  <title>Environment Variable Dashboard</title>
</head>

<body class="home">
  <div class="container">
    <div class="navbar">
      <h1 id="title">Environment Variable Dashboard</h1>
      <p id="credit">coded with &#10084;&#65039;&nbsp; by Infra Team</p>
    </div>
    <p>Please select one of the environment variable below</p>
    <?php
    $dir = '../env';
    $scan = scandir($dir);

    foreach ($scan as $file) {
      if ($file != '.' && $file != '..') {

        echo '<a href="page.php" onClick=selectFile("' . $file . '")>' . $file . '</a><br>';
      }
    }
    ?>
  </div>
  <script src="script.js"></script>
</body>

</html>