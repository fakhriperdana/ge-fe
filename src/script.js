async function fetchTextFile(input) {
    let envArr = [];
    let envDict = [];

    await fetch(input)
        .then(response => response.text())
        .then(function (data) {
            data = data.replace(/^\n/gm, "");

            for (let j = 0; j < data.split("\n").length; j++) {
                envArr.push(data.split("\n")[j]);
            }

            for (let k = 0; k < envArr.length; k++) {
                envDict.push({
                    var: envArr[k].split("=")[0],
                    value: envArr[k].split(/=(.+)/)[1]
                });
            }
        });

    return envDict;
}

function generateTableHead(data) {
    let thead = document.querySelector("table").createTHead();
    let row = thead.insertRow();

    for (let key of data) {
        let th = document.createElement("th");
        let text = document.createTextNode(key);

        th.setAttribute("scope", "col");
        th.appendChild(text);

        row.appendChild(th);
    }
}

function generateTableContent(inputDict) {
    let tbody = document.querySelector("table").createTBody();

    for (let element of inputDict) {
        let row = tbody.insertRow();

        if (element['var'].charAt(0) === "#") {
            row.classList.add("bg-danger");
            row.classList.add("text-white");
        }

        for (let value in element) {
            let td = row.insertCell();
            let text = document.createTextNode(element[value]);

            td.appendChild(text);
        }
    }
}

function varSort(envKey, inputDict) {
    document.querySelector('table').innerHTML = "";

    generateTableHead(envKey);

    let tbody = document.querySelector("table").createTBody();

    inputDict.sort(sortAZ);

    for (let element of inputDict) {
        let row = tbody.insertRow();

        if (element['var'].charAt(0) === "#") {
            row.classList.add("bg-danger");
            row.classList.add("text-white");
        }

        for (let value in element) {
            let td = row.insertCell();
            let text = document.createTextNode(element[value]);

            td.appendChild(text);
        }
    }

    //local function
    function sortAZ(a, b) {

        const varA = a.var;
        const varB = b.var;

        let comparison = 0;

        if (varA > varB) {
            comparison = 1;
        } else if (varA < varB) {
            comparison = -1;
        }

        return comparison;
    }
}

function varSortReversed(envKey, inputDict) {
    document.querySelector('table').innerHTML = "";

    generateTableHead(envKey);

    let tbody = document.querySelector("table").createTBody();

    inputDict.sort(sortZA);

    for (let element of inputDict) {
        let row = tbody.insertRow();

        if (element['var'].charAt(0) === "#") {
            row.classList.add("bg-danger");
            row.classList.add("text-white");
        }

        for (let value in element) {
            let td = row.insertCell();
            let text = document.createTextNode(element[value]);

            td.appendChild(text);
        }
    }

    //local function
    function sortZA(a, b) {

        const varA = a.var;
        const varB = b.var;

        let comparison = 0;

        if (varA > varB) {
            comparison = 1;
        } else if (varA < varB) {
            comparison = -1;
        }

        return comparison * -1;
    }
}

function selectFile(fileName) {
    sessionStorage.setItem('input', fileName);
    document.cookie = "filename=" + fileName;
}

async function main() {
    let envName = sessionStorage.getItem('input');

    let file = "../env/" + envName;

    let rawEnv = await fetchTextFile(file);

    if (document.querySelector("body").className === "page") {
        let ele = document.createElement("p");
        let text = document.createTextNode(envName);

        ele.appendChild(text);

        let x = document.querySelector(".file-name");
        x.appendChild(ele);

        let envKey = Object.keys(rawEnv[0]);

        generateTableHead(envKey);
        generateTableContent(rawEnv);

        document.getElementById('sort-button').addEventListener("click", function () { varSort(envKey, rawEnv) });
        document.getElementById('sort-button-reversed').addEventListener("click", function () { varSortReversed(envKey, rawEnv) });
    }
}

main();